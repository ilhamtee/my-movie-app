import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movie_app/app.dart';
import 'package:my_movie_app/cores/helpers/navigation_helper.dart';
import 'package:my_movie_app/cores/injector/injector.dart';
import 'package:my_movie_app/cores/route/route_generator.dart';
import 'package:my_movie_app/cores/route/route_name.dart';
import 'package:my_movie_app/cores/styles/app_colors.dart';
import 'package:my_movie_app/features/home/presentation/bloc/home_bloc.dart';

Future<void> main() async {
  // Initialize app configuration
  await App.initApp();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider(create: (_) => getIt<HomeBloc>()),
        ],
        child: MaterialApp(
          title: 'My Movie App',
          theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(
              seedColor: AppColors.primaryColor,
              primary: AppColors.primaryColor,
            ),
            fontFamily: 'Poppins',
            useMaterial3: true,
          ),
          debugShowCheckedModeBanner: false,
          navigatorKey: navigate.navigatorKey,
          initialRoute: RouteName.home,
          onGenerateRoute: RouteGenerator.generate,
        ));
  }
}
