import 'package:equatable/equatable.dart';
import 'package:my_movie_app/cores/helpers/date_format_helper.dart';

class MovieEntity extends Equatable {
  final String backdropPath;
  final int id;
  final String originalTitle;
  final String overview;
  final String posterPath;
  final String releaseDate;
  final String title;
  final double voteAverage;

  const MovieEntity({
    required this.backdropPath,
    required this.id,
    required this.originalTitle,
    required this.overview,
    required this.posterPath,
    required this.releaseDate,
    required this.title,
    required this.voteAverage,
  });

  String getFormattedReleaseDate() {
    DateTime? parsedDate = DateTime.tryParse(releaseDate);

    if (parsedDate != null) {
      return AppDateFormat.fullDate(parsedDate);
    }

    return '';
  }

  factory MovieEntity.fromJson(Map<String, dynamic> json) {
    return MovieEntity(
      backdropPath: json['backdropPath'] ?? '',
      id: json['id'] ?? 0,
      originalTitle: json['originalTitle'] ?? '',
      overview: json['overview'] ?? '',
      posterPath: json['posterPath'] ?? '',
      releaseDate: json['releaseDate'] ?? '',
      title: json['title'] ?? '',
      voteAverage:
          json['voteAverage'] != null ? json['voteAverage'].toDouble() : 0.0,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'backdropPath': backdropPath,
      'id': id,
      'originalTitle': originalTitle,
      'overview': overview,
      'posterPath': posterPath,
      'releaseDate': releaseDate,
      'title': title,
      'voteAverage': voteAverage,
    };
  }

  @override
  List<Object?> get props => [
        backdropPath,
        id,
        originalTitle,
        overview,
        posterPath,
        releaseDate,
        title,
        voteAverage,
      ];
}
