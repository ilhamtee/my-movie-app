import 'package:equatable/equatable.dart';
import 'package:my_movie_app/cores/helpers/date_format_helper.dart';

class DetailMovieEntity extends Equatable {
  final int id;
  final int budget;
  final int runtime;
  final String originalTitle;
  final String overview;
  final String posterPath;
  final String homepage;
  final String releaseDate;
  final String tagline;
  final String title;
  final double voteAverage;
  final double popularity;
  final List<GenreEntity> genres;
  final List<ProductionCompanyEntity> productionCompanies;

  const DetailMovieEntity({
    required this.id,
    required this.budget,
    required this.originalTitle,
    required this.overview,
    required this.popularity,
    required this.posterPath,
    required this.homepage,
    required this.genres,
    required this.productionCompanies,
    required this.releaseDate,
    required this.runtime,
    required this.tagline,
    required this.title,
    required this.voteAverage,
  });

  String getFormattedReleaseDate() {
    DateTime? parsedDate = DateTime.tryParse(releaseDate);

    if (parsedDate != null) {
      return AppDateFormat.fullDate(parsedDate);
    }

    return '';
  }

  @override
  List<Object?> get props => [
        budget,
        genres,
        id,
        originalTitle,
        overview,
        popularity,
        posterPath,
        productionCompanies,
        releaseDate,
        runtime,
        tagline,
        title,
        voteAverage,
        homepage,
      ];
}

class GenreEntity {
  final int id;
  final String name;

  const GenreEntity({required this.id, required this.name});

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
    };
  }
}

class ProductionCompanyEntity {
  final int id;
  final String logoPath;
  final String name;
  final String originCountry;

  const ProductionCompanyEntity({
    required this.id,
    required this.logoPath,
    required this.name,
    required this.originCountry,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'logoPath': logoPath,
      'name': name,
      'originCountry': originCountry,
    };
  }
}
