import 'package:dartz/dartz.dart';
import 'package:my_movie_app/cores/exception/exception.dart';
import 'package:my_movie_app/features/home/domain/entities/detail_movie_entity.dart';
import 'package:my_movie_app/features/home/domain/entities/movie_entity.dart';
import 'package:my_movie_app/features/home/domain/use_cases/fetch_popular_movies_use_case.dart';

abstract class HomeRepository {
  Future<Either<Failures, List<MovieEntity>>> fetchPopularMovies(
      {required MovieParam param});

  Future<Either<Failures, DetailMovieEntity>> fetchDetailMovie(
      {required int movieId});

  Future<Either<Failures, Unit>> saveMovies({
    required List<MovieEntity> movies,
  });

  Future<Either<Failures, List<MovieEntity>>> getSavedMovies();
}
