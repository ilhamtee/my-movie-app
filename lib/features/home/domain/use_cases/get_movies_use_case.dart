import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:my_movie_app/cores/exception/exception.dart';
import 'package:my_movie_app/cores/use_case/use_case.dart';
import 'package:my_movie_app/features/home/domain/entities/movie_entity.dart';
import 'package:my_movie_app/features/home/domain/repositories/home_repository.dart';

@injectable
class GetMoviesUseCase implements UseCase<List<MovieEntity>, NoParams> {
  final HomeRepository _repository;

  GetMoviesUseCase(this._repository);
  @override
  Future<Either<Failures, List<MovieEntity>>> call(NoParams param) async {
    return await _repository.getSavedMovies();
  }
}
