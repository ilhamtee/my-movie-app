import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:my_movie_app/cores/exception/exception.dart';
import 'package:my_movie_app/cores/use_case/use_case.dart';
import 'package:my_movie_app/features/home/domain/entities/detail_movie_entity.dart';
import 'package:my_movie_app/features/home/domain/repositories/home_repository.dart';

@injectable
class FetchDetailPopularMovieUseCase
    implements UseCase<DetailMovieEntity, int> {
  final HomeRepository _repository;

  FetchDetailPopularMovieUseCase(this._repository);
  @override
  Future<Either<Failures, DetailMovieEntity>> call(int movieId) async {
    return await _repository.fetchDetailMovie(movieId: movieId);
  }
}
