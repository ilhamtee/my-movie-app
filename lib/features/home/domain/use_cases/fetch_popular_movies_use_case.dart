import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';
import 'package:my_movie_app/cores/exception/exception.dart';
import 'package:my_movie_app/cores/use_case/use_case.dart';
import 'package:my_movie_app/features/home/domain/entities/movie_entity.dart';
import 'package:my_movie_app/features/home/domain/repositories/home_repository.dart';

@injectable
class FetchPopularMoviesUseCase
    implements UseCase<List<MovieEntity>, MovieParam> {
  final HomeRepository _repository;

  FetchPopularMoviesUseCase(this._repository);
  @override
  Future<Either<Failures, List<MovieEntity>>> call(MovieParam param) async {
    return await _repository.fetchPopularMovies(param: param);
  }
}

class MovieParam extends Equatable {
  final String? language;
  final int page;
  final String? region;

  const MovieParam({
    required this.page,
    this.language,
    this.region,
  });

  Map<String, dynamic> toJson() {
    return {
      'page': page,
      if (language != null) 'language': language,
      if (region != null) 'region': region,
    };
  }

  @override
  List<Object?> get props => [
        language,
        page,
        region,
      ];
}
