import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:my_movie_app/cores/exception/exception.dart';
import 'package:my_movie_app/cores/use_case/use_case.dart';
import 'package:my_movie_app/features/home/domain/entities/movie_entity.dart';
import 'package:my_movie_app/features/home/domain/repositories/home_repository.dart';

@injectable
class SaveMoviesUseCase implements UseCase<Unit, List<MovieEntity>> {
  final HomeRepository _repository;

  SaveMoviesUseCase(this._repository);
  @override
  Future<Either<Failures, Unit>> call(List<MovieEntity> movies) async {
    return await _repository.saveMovies(movies: movies);
  }
}
