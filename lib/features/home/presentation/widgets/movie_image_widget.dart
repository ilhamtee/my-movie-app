import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class MovieImageWidget extends StatelessWidget {
  const MovieImageWidget(
      {super.key, required this.imageUrl, this.widthImage, this.heightImage});

  final String imageUrl;
  final double? widthImage;
  final double? heightImage;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: const BorderRadius.all(Radius.circular(8)),
      child: CachedNetworkImage(
        imageUrl: imageUrl,
        width: widthImage ?? 150,
        height: heightImage ?? 200,
        fit: BoxFit.fill,
        placeholder: (context, url) => const Center(
          child: SizedBox.square(
              dimension: 30, child: CircularProgressIndicator()),
        ),
        errorWidget: (context, url, error) => Column(
          children: [
            const Icon(
              Icons.warning,
              size: 15,
              color: Colors.grey,
            ),
            Text(
              error.toString(),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.labelSmall?.copyWith(
                    fontSize: 10,
                    color: Colors.grey,
                  ),
            ),
          ],
        ),
      ),
    );
  }
}
