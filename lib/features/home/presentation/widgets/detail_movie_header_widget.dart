import 'package:flutter/material.dart';
import 'package:my_movie_app/cores/extensions/iterable_extension.dart';
import 'package:my_movie_app/cores/extensions/string_extension.dart';
import 'package:my_movie_app/cores/styles/app_colors.dart';
import 'package:my_movie_app/features/home/domain/entities/detail_movie_entity.dart';
import 'package:my_movie_app/features/home/presentation/widgets/movie_image_widget.dart';

class DetailMovieHeaderWidget extends StatelessWidget {
  const DetailMovieHeaderWidget({super.key, required this.detailMovie});
  final DetailMovieEntity detailMovie;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MovieImageWidget(imageUrl: detailMovie.posterPath.imageURL),
        const SizedBox(
          width: 10,
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                detailMovie.originalTitle,
                style: textTheme.headlineMedium?.copyWith(
                  color: AppColors.primaryColor,
                  fontWeight: FontWeight.w600,
                ),
              ),
              Wrap(
                children: detailMovie.genres
                    .mapIndexed(
                      (index, genre) => Text(
                        '${genre.name} ${index + 1 == detailMovie.genres.length ? '' : ' | '}',
                        style: textTheme.labelSmall?.copyWith(
                          color: Colors.grey.shade600,
                          fontSize: 10,
                        ),
                      ),
                    )
                    .toList(),
              ),
              const SizedBox(
                height: 16,
              ),
              Table(
                children: [
                  TableRow(children: [
                    Text(
                      'Release Date',
                      style: textTheme.titleSmall,
                    ),
                    Text(
                      detailMovie.getFormattedReleaseDate(),
                      style: textTheme.titleSmall,
                    ),
                  ]),
                  TableRow(children: [
                    Text(
                      'Duration',
                      style: textTheme.titleSmall,
                    ),
                    Text('${detailMovie.runtime} minute',
                        style: textTheme.titleSmall),
                  ]),
                  TableRow(children: [
                    Text(
                      'Popularity',
                      style: textTheme.titleSmall,
                    ),
                    Text(detailMovie.popularity.toString(),
                        style: textTheme.titleSmall),
                  ]),
                  TableRow(
                    children: [
                      Text(
                        'Rate',
                        style: textTheme.titleSmall,
                      ),
                      Row(
                        children: [
                          Text(
                              detailMovie.voteAverage
                                  .toStringAsFixed(1)
                                  .toString(),
                              style: textTheme.titleSmall),
                          const Icon(
                            Icons.star,
                            size: 15,
                            color: Colors.red,
                          ),
                        ],
                      )
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
