import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:my_movie_app/cores/extensions/either_extension.dart';
import 'package:my_movie_app/cores/use_case/use_case.dart';
import 'package:my_movie_app/features/home/domain/entities/detail_movie_entity.dart';
import 'package:my_movie_app/features/home/domain/entities/movie_entity.dart';
import 'package:my_movie_app/features/home/domain/use_cases/fetch_detail_popular_movie_use_case.dart';
import 'package:my_movie_app/features/home/domain/use_cases/fetch_popular_movies_use_case.dart';
import 'package:my_movie_app/features/home/domain/use_cases/get_movies_use_case.dart';
import 'package:my_movie_app/features/home/domain/use_cases/save_movies_use_case.dart';

part 'home_event.dart';
part 'home_state.dart';
part 'home_bloc.freezed.dart';

@injectable
class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc(
    this._fetchPopularMoviesUseCase,
    this._fetchDetailPopularMovieUseCase,
    this._saveMoviesUseCase,
    this._getMoviesUseCase,
  ) : super(const HomeState()) {
    on<FetchPopularMoviesEvent>(_onFetchPopularMovies);
    on<FetchDetailMovieEvent>(_onFetchDetailMovie);
  }

  final FetchPopularMoviesUseCase _fetchPopularMoviesUseCase;
  final FetchDetailPopularMovieUseCase _fetchDetailPopularMovieUseCase;
  final SaveMoviesUseCase _saveMoviesUseCase;
  final GetMoviesUseCase _getMoviesUseCase;

  void _onFetchPopularMovies(FetchPopularMoviesEvent event, emit) async {
    emit(state.copyWith(status: HomeStateStatus.loading));

    final resultFetchPopularMovies =
        await _fetchPopularMoviesUseCase(event.param);

    final resultGetSavedMovies = await _getMoviesUseCase(NoParams());

    resultFetchPopularMovies.fold((failure) async {
      /// Handle get movies from local storage when user is offline
      if (resultGetSavedMovies.isRight()) {
        emit(state.copyWith(
          status: HomeStateStatus.success,
          popularMovieList: resultGetSavedMovies.getRight(),
        ));
      } else {
        emit(state.copyWith(
          status: HomeStateStatus.error,
          message: failure.errorMessage,
        ));
      }
    }, (popularMovies) async {
      emit(
        state.copyWith(
          status: HomeStateStatus.success,
          popularMovieList: popularMovies,
          currentPage: event.param.page,
        ),
      );

      await _saveMoviesUseCase(popularMovies);
    });
  }

  void _onFetchDetailMovie(FetchDetailMovieEvent event, emit) async {
    emit(state.copyWith(status: HomeStateStatus.loading));

    final resultFetchDetailMovie =
        await _fetchDetailPopularMovieUseCase(event.movieId);

    resultFetchDetailMovie.fold(
      (failure) {
        /// Handle detail movie when user is offline
        final MovieEntity selectedMovie = state.popularMovieList
            .firstWhere((element) => element.id == event.movieId);
        final DetailMovieEntity detailMovie = DetailMovieEntity(
          id: selectedMovie.id,
          budget: 0,
          originalTitle: selectedMovie.originalTitle,
          overview: selectedMovie.overview,
          popularity: 0.0,
          posterPath: selectedMovie.posterPath,
          homepage: '',
          genres: const [GenreEntity(id: 0, name: '-')],
          productionCompanies: const [],
          releaseDate: selectedMovie.releaseDate,
          runtime: 0,
          tagline: 'No Internet Connection',
          title: selectedMovie.title,
          voteAverage: selectedMovie.voteAverage,
        );

        emit(state.copyWith(
          status: HomeStateStatus.success,
          detailPopularMovieEntity: detailMovie,
        ));
      },
      (detailMovie) => emit(
        state.copyWith(
          status: HomeStateStatus.success,
          detailPopularMovieEntity: detailMovie,
        ),
      ),
    );
  }
}
