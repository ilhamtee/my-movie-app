part of 'home_bloc.dart';

@freezed
class HomeEvent with _$HomeEvent {
  const factory HomeEvent.fetchPopularMovies({required MovieParam param}) =
      FetchPopularMoviesEvent;
  const factory HomeEvent.fetchDetailMovie({required int movieId}) =
      FetchDetailMovieEvent;
}
