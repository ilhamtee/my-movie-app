part of 'home_bloc.dart';

enum HomeStateStatus {
  initial,
  loading,
  success,
  error;
}

@freezed
class HomeState with _$HomeState {
  const factory HomeState({
    @Default([]) List<MovieEntity> popularMovieList,
    @Default(0) int currentPage,
    @Default(HomeStateStatus.initial) HomeStateStatus status,
    DetailMovieEntity? detailPopularMovieEntity,
    @Default('') String message,
  }) = _HomeStateInitial;
}
