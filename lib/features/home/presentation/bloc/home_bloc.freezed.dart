// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'home_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$HomeEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(MovieParam param) fetchPopularMovies,
    required TResult Function(int movieId) fetchDetailMovie,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(MovieParam param)? fetchPopularMovies,
    TResult? Function(int movieId)? fetchDetailMovie,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(MovieParam param)? fetchPopularMovies,
    TResult Function(int movieId)? fetchDetailMovie,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchPopularMoviesEvent value) fetchPopularMovies,
    required TResult Function(FetchDetailMovieEvent value) fetchDetailMovie,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(FetchPopularMoviesEvent value)? fetchPopularMovies,
    TResult? Function(FetchDetailMovieEvent value)? fetchDetailMovie,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchPopularMoviesEvent value)? fetchPopularMovies,
    TResult Function(FetchDetailMovieEvent value)? fetchDetailMovie,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomeEventCopyWith<$Res> {
  factory $HomeEventCopyWith(HomeEvent value, $Res Function(HomeEvent) then) =
      _$HomeEventCopyWithImpl<$Res, HomeEvent>;
}

/// @nodoc
class _$HomeEventCopyWithImpl<$Res, $Val extends HomeEvent>
    implements $HomeEventCopyWith<$Res> {
  _$HomeEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$FetchPopularMoviesEventImplCopyWith<$Res> {
  factory _$$FetchPopularMoviesEventImplCopyWith(
          _$FetchPopularMoviesEventImpl value,
          $Res Function(_$FetchPopularMoviesEventImpl) then) =
      __$$FetchPopularMoviesEventImplCopyWithImpl<$Res>;
  @useResult
  $Res call({MovieParam param});
}

/// @nodoc
class __$$FetchPopularMoviesEventImplCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res, _$FetchPopularMoviesEventImpl>
    implements _$$FetchPopularMoviesEventImplCopyWith<$Res> {
  __$$FetchPopularMoviesEventImplCopyWithImpl(
      _$FetchPopularMoviesEventImpl _value,
      $Res Function(_$FetchPopularMoviesEventImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? param = null,
  }) {
    return _then(_$FetchPopularMoviesEventImpl(
      param: null == param
          ? _value.param
          : param // ignore: cast_nullable_to_non_nullable
              as MovieParam,
    ));
  }
}

/// @nodoc

class _$FetchPopularMoviesEventImpl implements FetchPopularMoviesEvent {
  const _$FetchPopularMoviesEventImpl({required this.param});

  @override
  final MovieParam param;

  @override
  String toString() {
    return 'HomeEvent.fetchPopularMovies(param: $param)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FetchPopularMoviesEventImpl &&
            (identical(other.param, param) || other.param == param));
  }

  @override
  int get hashCode => Object.hash(runtimeType, param);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FetchPopularMoviesEventImplCopyWith<_$FetchPopularMoviesEventImpl>
      get copyWith => __$$FetchPopularMoviesEventImplCopyWithImpl<
          _$FetchPopularMoviesEventImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(MovieParam param) fetchPopularMovies,
    required TResult Function(int movieId) fetchDetailMovie,
  }) {
    return fetchPopularMovies(param);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(MovieParam param)? fetchPopularMovies,
    TResult? Function(int movieId)? fetchDetailMovie,
  }) {
    return fetchPopularMovies?.call(param);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(MovieParam param)? fetchPopularMovies,
    TResult Function(int movieId)? fetchDetailMovie,
    required TResult orElse(),
  }) {
    if (fetchPopularMovies != null) {
      return fetchPopularMovies(param);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchPopularMoviesEvent value) fetchPopularMovies,
    required TResult Function(FetchDetailMovieEvent value) fetchDetailMovie,
  }) {
    return fetchPopularMovies(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(FetchPopularMoviesEvent value)? fetchPopularMovies,
    TResult? Function(FetchDetailMovieEvent value)? fetchDetailMovie,
  }) {
    return fetchPopularMovies?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchPopularMoviesEvent value)? fetchPopularMovies,
    TResult Function(FetchDetailMovieEvent value)? fetchDetailMovie,
    required TResult orElse(),
  }) {
    if (fetchPopularMovies != null) {
      return fetchPopularMovies(this);
    }
    return orElse();
  }
}

abstract class FetchPopularMoviesEvent implements HomeEvent {
  const factory FetchPopularMoviesEvent({required final MovieParam param}) =
      _$FetchPopularMoviesEventImpl;

  MovieParam get param;
  @JsonKey(ignore: true)
  _$$FetchPopularMoviesEventImplCopyWith<_$FetchPopularMoviesEventImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$FetchDetailMovieEventImplCopyWith<$Res> {
  factory _$$FetchDetailMovieEventImplCopyWith(
          _$FetchDetailMovieEventImpl value,
          $Res Function(_$FetchDetailMovieEventImpl) then) =
      __$$FetchDetailMovieEventImplCopyWithImpl<$Res>;
  @useResult
  $Res call({int movieId});
}

/// @nodoc
class __$$FetchDetailMovieEventImplCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res, _$FetchDetailMovieEventImpl>
    implements _$$FetchDetailMovieEventImplCopyWith<$Res> {
  __$$FetchDetailMovieEventImplCopyWithImpl(_$FetchDetailMovieEventImpl _value,
      $Res Function(_$FetchDetailMovieEventImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? movieId = null,
  }) {
    return _then(_$FetchDetailMovieEventImpl(
      movieId: null == movieId
          ? _value.movieId
          : movieId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$FetchDetailMovieEventImpl implements FetchDetailMovieEvent {
  const _$FetchDetailMovieEventImpl({required this.movieId});

  @override
  final int movieId;

  @override
  String toString() {
    return 'HomeEvent.fetchDetailMovie(movieId: $movieId)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FetchDetailMovieEventImpl &&
            (identical(other.movieId, movieId) || other.movieId == movieId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, movieId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FetchDetailMovieEventImplCopyWith<_$FetchDetailMovieEventImpl>
      get copyWith => __$$FetchDetailMovieEventImplCopyWithImpl<
          _$FetchDetailMovieEventImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(MovieParam param) fetchPopularMovies,
    required TResult Function(int movieId) fetchDetailMovie,
  }) {
    return fetchDetailMovie(movieId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(MovieParam param)? fetchPopularMovies,
    TResult? Function(int movieId)? fetchDetailMovie,
  }) {
    return fetchDetailMovie?.call(movieId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(MovieParam param)? fetchPopularMovies,
    TResult Function(int movieId)? fetchDetailMovie,
    required TResult orElse(),
  }) {
    if (fetchDetailMovie != null) {
      return fetchDetailMovie(movieId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchPopularMoviesEvent value) fetchPopularMovies,
    required TResult Function(FetchDetailMovieEvent value) fetchDetailMovie,
  }) {
    return fetchDetailMovie(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(FetchPopularMoviesEvent value)? fetchPopularMovies,
    TResult? Function(FetchDetailMovieEvent value)? fetchDetailMovie,
  }) {
    return fetchDetailMovie?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchPopularMoviesEvent value)? fetchPopularMovies,
    TResult Function(FetchDetailMovieEvent value)? fetchDetailMovie,
    required TResult orElse(),
  }) {
    if (fetchDetailMovie != null) {
      return fetchDetailMovie(this);
    }
    return orElse();
  }
}

abstract class FetchDetailMovieEvent implements HomeEvent {
  const factory FetchDetailMovieEvent({required final int movieId}) =
      _$FetchDetailMovieEventImpl;

  int get movieId;
  @JsonKey(ignore: true)
  _$$FetchDetailMovieEventImplCopyWith<_$FetchDetailMovieEventImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$HomeState {
  List<MovieEntity> get popularMovieList => throw _privateConstructorUsedError;
  int get currentPage => throw _privateConstructorUsedError;
  HomeStateStatus get status => throw _privateConstructorUsedError;
  DetailMovieEntity? get detailPopularMovieEntity =>
      throw _privateConstructorUsedError;
  String get message => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $HomeStateCopyWith<HomeState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomeStateCopyWith<$Res> {
  factory $HomeStateCopyWith(HomeState value, $Res Function(HomeState) then) =
      _$HomeStateCopyWithImpl<$Res, HomeState>;
  @useResult
  $Res call(
      {List<MovieEntity> popularMovieList,
      int currentPage,
      HomeStateStatus status,
      DetailMovieEntity? detailPopularMovieEntity,
      String message});
}

/// @nodoc
class _$HomeStateCopyWithImpl<$Res, $Val extends HomeState>
    implements $HomeStateCopyWith<$Res> {
  _$HomeStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? popularMovieList = null,
    Object? currentPage = null,
    Object? status = null,
    Object? detailPopularMovieEntity = freezed,
    Object? message = null,
  }) {
    return _then(_value.copyWith(
      popularMovieList: null == popularMovieList
          ? _value.popularMovieList
          : popularMovieList // ignore: cast_nullable_to_non_nullable
              as List<MovieEntity>,
      currentPage: null == currentPage
          ? _value.currentPage
          : currentPage // ignore: cast_nullable_to_non_nullable
              as int,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as HomeStateStatus,
      detailPopularMovieEntity: freezed == detailPopularMovieEntity
          ? _value.detailPopularMovieEntity
          : detailPopularMovieEntity // ignore: cast_nullable_to_non_nullable
              as DetailMovieEntity?,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$HomeStateInitialImplCopyWith<$Res>
    implements $HomeStateCopyWith<$Res> {
  factory _$$HomeStateInitialImplCopyWith(_$HomeStateInitialImpl value,
          $Res Function(_$HomeStateInitialImpl) then) =
      __$$HomeStateInitialImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<MovieEntity> popularMovieList,
      int currentPage,
      HomeStateStatus status,
      DetailMovieEntity? detailPopularMovieEntity,
      String message});
}

/// @nodoc
class __$$HomeStateInitialImplCopyWithImpl<$Res>
    extends _$HomeStateCopyWithImpl<$Res, _$HomeStateInitialImpl>
    implements _$$HomeStateInitialImplCopyWith<$Res> {
  __$$HomeStateInitialImplCopyWithImpl(_$HomeStateInitialImpl _value,
      $Res Function(_$HomeStateInitialImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? popularMovieList = null,
    Object? currentPage = null,
    Object? status = null,
    Object? detailPopularMovieEntity = freezed,
    Object? message = null,
  }) {
    return _then(_$HomeStateInitialImpl(
      popularMovieList: null == popularMovieList
          ? _value._popularMovieList
          : popularMovieList // ignore: cast_nullable_to_non_nullable
              as List<MovieEntity>,
      currentPage: null == currentPage
          ? _value.currentPage
          : currentPage // ignore: cast_nullable_to_non_nullable
              as int,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as HomeStateStatus,
      detailPopularMovieEntity: freezed == detailPopularMovieEntity
          ? _value.detailPopularMovieEntity
          : detailPopularMovieEntity // ignore: cast_nullable_to_non_nullable
              as DetailMovieEntity?,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$HomeStateInitialImpl implements _HomeStateInitial {
  const _$HomeStateInitialImpl(
      {final List<MovieEntity> popularMovieList = const [],
      this.currentPage = 0,
      this.status = HomeStateStatus.initial,
      this.detailPopularMovieEntity,
      this.message = ''})
      : _popularMovieList = popularMovieList;

  final List<MovieEntity> _popularMovieList;
  @override
  @JsonKey()
  List<MovieEntity> get popularMovieList {
    if (_popularMovieList is EqualUnmodifiableListView)
      return _popularMovieList;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_popularMovieList);
  }

  @override
  @JsonKey()
  final int currentPage;
  @override
  @JsonKey()
  final HomeStateStatus status;
  @override
  final DetailMovieEntity? detailPopularMovieEntity;
  @override
  @JsonKey()
  final String message;

  @override
  String toString() {
    return 'HomeState(popularMovieList: $popularMovieList, currentPage: $currentPage, status: $status, detailPopularMovieEntity: $detailPopularMovieEntity, message: $message)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$HomeStateInitialImpl &&
            const DeepCollectionEquality()
                .equals(other._popularMovieList, _popularMovieList) &&
            (identical(other.currentPage, currentPage) ||
                other.currentPage == currentPage) &&
            (identical(other.status, status) || other.status == status) &&
            (identical(
                    other.detailPopularMovieEntity, detailPopularMovieEntity) ||
                other.detailPopularMovieEntity == detailPopularMovieEntity) &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_popularMovieList),
      currentPage,
      status,
      detailPopularMovieEntity,
      message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$HomeStateInitialImplCopyWith<_$HomeStateInitialImpl> get copyWith =>
      __$$HomeStateInitialImplCopyWithImpl<_$HomeStateInitialImpl>(
          this, _$identity);
}

abstract class _HomeStateInitial implements HomeState {
  const factory _HomeStateInitial(
      {final List<MovieEntity> popularMovieList,
      final int currentPage,
      final HomeStateStatus status,
      final DetailMovieEntity? detailPopularMovieEntity,
      final String message}) = _$HomeStateInitialImpl;

  @override
  List<MovieEntity> get popularMovieList;
  @override
  int get currentPage;
  @override
  HomeStateStatus get status;
  @override
  DetailMovieEntity? get detailPopularMovieEntity;
  @override
  String get message;
  @override
  @JsonKey(ignore: true)
  _$$HomeStateInitialImplCopyWith<_$HomeStateInitialImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
