import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movie_app/cores/helpers/navigation_helper.dart';
import 'package:my_movie_app/cores/route/route_name.dart';
import 'package:my_movie_app/cores/styles/app_colors.dart';
import 'package:my_movie_app/features/home/domain/use_cases/fetch_popular_movies_use_case.dart';
import 'package:my_movie_app/features/home/presentation/bloc/home_bloc.dart';
import 'package:my_movie_app/features/home/presentation/widgets/popular_movie_item_widget.dart';
import 'package:my_movie_app/features/home/presentation/widgets/search_movie_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late final HomeBloc _homeBloc = context.read<HomeBloc>();

  final _searchQueryController = TextEditingController();

  @override
  void initState() {
    _homeBloc.add(const FetchPopularMoviesEvent(param: MovieParam(page: 1)));
    super.initState();
  }

  @override
  void dispose() {
    _searchQueryController.dispose();
    _homeBloc.close();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.primaryColor,
        title: const Text(
          'My Movie App',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
      backgroundColor: Colors.white,
      body: BlocBuilder<HomeBloc, HomeState>(
        builder: (context, state) {
          if (state.status == HomeStateStatus.loading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          return SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                  child: SearchMovieWidget(
                    controller: _searchQueryController,
                    onChanged: (value) {},
                  ),
                ),
                const SizedBox(
                  height: 12,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Text(
                    'Popular Movies',
                    style: Theme.of(context).textTheme.titleLarge?.copyWith(
                          color: AppColors.primaryColor,
                          fontWeight: FontWeight.w600,
                        ),
                  ),
                ),
                ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: state.popularMovieList.length,
                    itemExtent: 120,
                    shrinkWrap: true,
                    itemBuilder: (_, index) {
                      final movie = state.popularMovieList[index];
                      return PopularMovieItem(
                        movie: movie,
                        onTap: () => navigate.pushTo(
                          RouteName.detailMovie,
                          data: movie.id,
                        ),
                      );
                    }),
              ],
            ),
          );
        },
      ),
    );
  }
}
