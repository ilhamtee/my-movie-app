import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movie_app/cores/helpers/navigation_helper.dart';
import 'package:my_movie_app/cores/styles/app_colors.dart';
import 'package:my_movie_app/features/home/presentation/bloc/home_bloc.dart';
import 'package:my_movie_app/features/home/presentation/widgets/detail_movie_header_widget.dart';

class DetailMoviePage extends StatefulWidget {
  const DetailMoviePage({super.key, required this.movieId});

  final int movieId;

  @override
  State<DetailMoviePage> createState() => _DetailMoviePageState();
}

class _DetailMoviePageState extends State<DetailMoviePage> {
  late final HomeBloc _homeBloc = context.read<HomeBloc>();

  @override
  void initState() {
    _homeBloc.add(FetchDetailMovieEvent(movieId: widget.movieId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (context, state) {
        return Scaffold(
          backgroundColor: Colors.white,
          appBar: state.status == HomeStateStatus.loading
              ? null
              : AppBar(
                  backgroundColor: AppColors.primaryColor,
                  title: Text(
                    state.detailPopularMovieEntity?.title ?? '',
                    style: textTheme.titleMedium?.copyWith(color: Colors.white),
                  ),
                  leading: IconButton(
                    icon: const Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                    ),
                    onPressed: () => navigate.pop(),
                  ),
                ),
          body: state.status == HomeStateStatus.loading
              ? const Center(
                  child: SizedBox.square(
                    dimension: 40,
                    child: CircularProgressIndicator(),
                  ),
                )
              : state.status == HomeStateStatus.success &&
                      state.detailPopularMovieEntity != null
                  ? Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 12),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          DetailMovieHeaderWidget(
                            detailMovie: state.detailPopularMovieEntity!,
                          ),
                          const SizedBox(
                            height: 24,
                          ),
                          Text(
                            'OVERVIEW',
                            style: textTheme.titleLarge?.copyWith(
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          Text(
                            state.detailPopularMovieEntity!.overview,
                          ),
                          const SizedBox(
                            height: 32,
                          ),
                          Center(
                            child: Text(
                              '-${state.detailPopularMovieEntity!.tagline}-',
                              style: textTheme.labelSmall?.copyWith(
                                fontStyle: FontStyle.italic,
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  : const SizedBox.shrink(),
        );
      },
    );
  }
}
