import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:my_movie_app/cores/constants/url_constant.dart';
import 'package:my_movie_app/cores/exception/exception.dart';
import 'package:my_movie_app/cores/extensions/dio_response_extension.dart';
import 'package:my_movie_app/cores/helpers/dio_helper.dart';
import 'package:my_movie_app/features/home/data/data_sources/remote/home_remote_data_source.dart';
import 'package:my_movie_app/features/home/data/models/detail_movie_model.dart';
import 'package:my_movie_app/features/home/data/models/movie_model.dart';
import 'package:my_movie_app/features/home/domain/use_cases/fetch_popular_movies_use_case.dart';

@LazySingleton(as: HomeRemoteDataSource)
class HomeRemoteDataSourceImpl implements HomeRemoteDataSource {
  final Dio _dio;

  HomeRemoteDataSourceImpl(this._dio);

  @override
  Future<Either<Failures, List<MovieModel>>> fetchPopularMovies(
      {required MovieParam param}) async {
    try {
      final response = await _dio.get(
        URLConstant.popularMovies,
        queryParameters: param.toJson(),
      );

      if (response.isSuccess) {
        final List<dynamic> rawPopularMovies = response.data['results'];

        final List<MovieModel> popularMovies =
            rawPopularMovies.map((e) => MovieModel.fromJson(e)).toList();

        return Right(popularMovies);
      }

      return const Right([]);
    } on DioException catch (e) {
      final message = DioHelper.generalException(e);
      return Left(ServerFailure(errorMessage: message));
    } catch (e) {
      return Left(UnexpectedFailure(errorMessage: e.toString()));
    }
  }

  @override
  Future<Either<Failures, DetailMovieModel>> fetchDetailMovie(
      {required int movieId}) async {
    try {
      final response = await _dio.get(
        '${URLConstant.baseURL}/$movieId',
      );

      if (response.isSuccess) {
        final rawDetailMovie = response.data;

        final DetailMovieModel detailMovie =
            DetailMovieModel.fromJson(rawDetailMovie);

        return Right(detailMovie);
      }

      return Right(DetailMovieModel.empty());
    } on DioException catch (e) {
      final message = DioHelper.generalException(e);
      return Left(ServerFailure(errorMessage: message));
    } catch (e) {
      return Left(UnexpectedFailure(errorMessage: e.toString()));
    }
  }
}
