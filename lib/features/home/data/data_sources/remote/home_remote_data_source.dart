import 'package:dartz/dartz.dart';
import 'package:my_movie_app/cores/exception/exception.dart';
import 'package:my_movie_app/features/home/data/models/detail_movie_model.dart';
import 'package:my_movie_app/features/home/data/models/movie_model.dart';
import 'package:my_movie_app/features/home/domain/use_cases/fetch_popular_movies_use_case.dart';

abstract class HomeRemoteDataSource {
  Future<Either<Failures, List<MovieModel>>> fetchPopularMovies(
      {required MovieParam param});

  Future<Either<Failures, DetailMovieModel>> fetchDetailMovie(
      {required int movieId});
}
