import 'package:dartz/dartz.dart';
import 'package:my_movie_app/cores/exception/exception.dart';
import 'package:my_movie_app/features/home/data/models/movie_model.dart';

abstract class HomeLocalDataSource {
  Future<Either<Failures, Unit>> saveMovies({
    required List<MovieModel> movies,
  });

  Future<Either<Failures, List<MovieModel>>> getSavedMovies();
}
