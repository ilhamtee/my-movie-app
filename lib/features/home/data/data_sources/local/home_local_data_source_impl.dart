import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:my_movie_app/cores/constants/storage_constant.dart';
import 'package:my_movie_app/cores/exception/exception.dart';
import 'package:my_movie_app/cores/helpers/storage_helper.dart';
import 'package:my_movie_app/features/home/data/data_sources/local/home_local_data_source.dart';
import 'package:my_movie_app/features/home/data/models/movie_model.dart';

@LazySingleton(as: HomeLocalDataSource)
class HomeLocalDataSourceImpl implements HomeLocalDataSource {
  final shared = StorageHelper<String>();

  @override
  Future<Either<Failures, List<MovieModel>>> getSavedMovies() async {
    try {
      List<MovieModel> savedMovies = [];
      String? rawMovies = await shared.read(StorageConstant.listMovieKey);

      if (rawMovies != null) {
        List<dynamic> decodedMovies = jsonDecode(rawMovies);
        savedMovies =
            decodedMovies.map((movie) => MovieModel.fromJson(movie)).toList();
      }

      return Right(savedMovies);
    } catch (e) {
      return Left(DatabaseFailure(errorMessage: e.toString()));
    }
  }

  @override
  Future<Either<Failures, Unit>> saveMovies(
      {required List<MovieModel> movies}) async {
    try {
      List<Map<String, dynamic>> mapMovies =
          movies.map((movie) => movie.toJson()).toList();

      String stringMovies = jsonEncode(mapMovies);

      await shared.store(StorageConstant.listMovieKey, stringMovies);

      return const Right(unit);
    } catch (e) {
      return Left(DatabaseFailure(errorMessage: e.toString()));
    }
  }
}
