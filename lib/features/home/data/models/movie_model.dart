import 'package:json_annotation/json_annotation.dart';
import 'package:my_movie_app/features/home/domain/entities/movie_entity.dart';

part 'movie_model.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class MovieModel extends MovieEntity {
  const MovieModel({
    String? backdropPath,
    int? id,
    String? originalTitle,
    String? overview,
    String? posterPath,
    String? releaseDate,
    String? title,
    double? voteAverage,
  }) : super(
          backdropPath: backdropPath ?? '',
          id: id ?? 0,
          title: title ?? '',
          originalTitle: originalTitle ?? '',
          overview: overview ?? '',
          posterPath: posterPath ?? '',
          releaseDate: releaseDate ?? '',
          voteAverage: voteAverage ?? 0.0,
        );

  factory MovieModel.fromJson(Map<String, dynamic> json) =>
      _$MovieModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$MovieModelToJson(this);
}
