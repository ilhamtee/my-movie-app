import 'package:json_annotation/json_annotation.dart';
import 'package:my_movie_app/features/home/domain/entities/detail_movie_entity.dart';

part 'detail_movie_model.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class DetailMovieModel extends DetailMovieEntity {
  DetailMovieModel({
    int? id,
    int? budget,
    int? runtime,
    String? originalTitle,
    String? overview,
    String? posterPath,
    String? homepage,
    String? releaseDate,
    String? tagline,
    String? title,
    double? voteAverage,
    double? popularity,
    List<GenreModel>? genres,
    List<ProductionCompanyModel>? productionCompanies,
  }) : super(
          id: id ?? 0,
          budget: budget ?? 0,
          runtime: runtime ?? 0,
          originalTitle: originalTitle ?? '',
          overview: overview ?? '',
          posterPath: posterPath ?? '',
          homepage: homepage ?? '',
          releaseDate: releaseDate ?? '',
          tagline: tagline ?? '',
          title: title ?? '',
          voteAverage: voteAverage ?? 0.0,
          popularity: popularity ?? 0.0,
          genres: genres ?? [],
          productionCompanies: productionCompanies ?? [],
        );

  factory DetailMovieModel.fromJson(Map<String, dynamic> json) =>
      _$DetailMovieModelFromJson(json);

  Map<String, dynamic> toJson() => _$DetailMovieModelToJson(this);

  static DetailMovieModel empty() {
    return DetailMovieModel(
      budget: 0,
      genres: const [],
      homepage: '',
      id: 0,
      originalTitle: '',
      overview: '',
      popularity: 0.0,
      posterPath: '',
      productionCompanies: const [],
      releaseDate: '',
      runtime: 0,
      tagline: '',
      title: '',
      voteAverage: 0.0,
    );
  }
}

@JsonSerializable(fieldRename: FieldRename.snake)
class GenreModel extends GenreEntity {
  GenreModel({
    int? id,
    String? name,
  }) : super(
          id: id ?? 0,
          name: name ?? '',
        );

  factory GenreModel.fromJson(Map<String, dynamic> json) =>
      _$GenreModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$GenreModelToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class ProductionCompanyModel extends ProductionCompanyEntity {
  const ProductionCompanyModel({
    int? id,
    String? logoPath,
    String? name,
    String? originCountry,
  }) : super(
          id: id ?? 0,
          logoPath: logoPath ?? '',
          name: name ?? '',
          originCountry: originCountry ?? '',
        );

  factory ProductionCompanyModel.fromJson(Map<String, dynamic> json) =>
      _$ProductionCompanyModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$ProductionCompanyModelToJson(this);
}
