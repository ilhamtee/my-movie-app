import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:my_movie_app/cores/exception/exception.dart';
import 'package:my_movie_app/features/home/data/data_sources/local/home_local_data_source.dart';
import 'package:my_movie_app/features/home/data/data_sources/remote/home_remote_data_source.dart';
import 'package:my_movie_app/features/home/data/models/detail_movie_model.dart';
import 'package:my_movie_app/features/home/data/models/movie_model.dart';
import 'package:my_movie_app/features/home/domain/entities/movie_entity.dart';
import 'package:my_movie_app/features/home/domain/repositories/home_repository.dart';
import 'package:my_movie_app/features/home/domain/use_cases/fetch_popular_movies_use_case.dart';

@LazySingleton(as: HomeRepository)
class HomeRepositoryImpl implements HomeRepository {
  final HomeRemoteDataSource _remoteDataSource;
  final HomeLocalDataSource _localDataSource;

  HomeRepositoryImpl(this._remoteDataSource, this._localDataSource);

  @override
  Future<Either<Failures, List<MovieModel>>> fetchPopularMovies(
      {required MovieParam param}) async {
    final result = await _remoteDataSource.fetchPopularMovies(param: param);

    return result.fold(
        (failures) => Left(failures), (moviesModel) => Right(moviesModel));
  }

  @override
  Future<Either<Failures, DetailMovieModel>> fetchDetailMovie(
      {required int movieId}) async {
    final result = await _remoteDataSource.fetchDetailMovie(movieId: movieId);

    return result.fold((failures) => Left(failures),
        (detailMovieModel) => Right(detailMovieModel));
  }

  @override
  Future<Either<Failures, List<MovieModel>>> getSavedMovies() async {
    final result = await _localDataSource.getSavedMovies();

    return result.fold(
        (failures) => Left(failures), (moviesModel) => Right(moviesModel));
  }

  @override
  Future<Either<Failures, Unit>> saveMovies({
    required List<MovieEntity> movies,
  }) async {
    final mappedMoviesModel = movies
        .map((e) => MovieModel(
            backdropPath: e.backdropPath,
            title: e.title,
            id: e.id,
            originalTitle: e.originalTitle,
            overview: e.overview,
            posterPath: e.posterPath,
            releaseDate: e.releaseDate,
            voteAverage: e.voteAverage))
        .toList();

    final result = await _localDataSource.saveMovies(movies: mappedMoviesModel);

    return result.fold((failures) => Left(failures), (r) => Right(r));
  }
}
