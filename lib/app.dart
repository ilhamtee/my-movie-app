import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:my_movie_app/cores/constants/url_constant.dart';
import 'package:my_movie_app/cores/helpers/dio_helper.dart';
import 'package:my_movie_app/cores/injector/injector.dart';

class App {
  App._();

  static Future<void> initApp() async {
    await dotenv.load(fileName: 'api_key.env');
    final apiKey = dotenv.env['API_KEY'];

    // Initialize Dependency Injection
    await configureDependencies();

    DioHelper.initialDio(URLConstant.baseURL);
    DioHelper.setDioTokenHeader(apiKey);
  }
}
