import 'package:intl/intl.dart';

class AppDateFormat {
  AppDateFormat._();

  static String fullDate(DateTime date) {
    return DateFormat('dd MMM yyyy').format(date);
  }
}
