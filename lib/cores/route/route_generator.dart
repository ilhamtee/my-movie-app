import 'package:flutter/material.dart';
import 'package:my_movie_app/cores/route/route_name.dart';
import 'package:my_movie_app/features/home/presentation/pages/detail_movie_page.dart';
import 'package:my_movie_app/features/home/presentation/pages/home_page.dart';

class RouteGenerator {
  RouteGenerator._();

  /// Initializing route
  static Route<dynamic>? generate(RouteSettings settings) {
    /// Declaring argument route
    final arguments = settings.arguments;

    switch (settings.name) {
      case RouteName.home:
        return MaterialPageRoute(
          builder: (_) => const HomePage(),
          settings: const RouteSettings(
            name: RouteName.home,
          ),
        );

      case RouteName.detailMovie:
        if (arguments is int) {
          return MaterialPageRoute(
            builder: (_) => DetailMoviePage(movieId: arguments),
            settings: const RouteSettings(
              name: RouteName.detailMovie,
            ),
          );
        }
    }

    return null;
  }
}
