// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i3;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:my_movie_app/cores/helpers/dio_helper.dart' as _i4;
import 'package:my_movie_app/cores/modules/network_module.dart' as _i16;
import 'package:my_movie_app/features/home/data/data_sources/local/home_local_data_source.dart'
    as _i5;
import 'package:my_movie_app/features/home/data/data_sources/local/home_local_data_source_impl.dart'
    as _i6;
import 'package:my_movie_app/features/home/data/data_sources/remote/home_remote_data_source.dart'
    as _i7;
import 'package:my_movie_app/features/home/data/data_sources/remote/home_remote_data_source_impl.dart'
    as _i8;
import 'package:my_movie_app/features/home/data/repositories/home_repository_impl.dart'
    as _i10;
import 'package:my_movie_app/features/home/domain/repositories/home_repository.dart'
    as _i9;
import 'package:my_movie_app/features/home/domain/use_cases/fetch_detail_popular_movie_use_case.dart'
    as _i12;
import 'package:my_movie_app/features/home/domain/use_cases/fetch_popular_movies_use_case.dart'
    as _i13;
import 'package:my_movie_app/features/home/domain/use_cases/get_movies_use_case.dart'
    as _i14;
import 'package:my_movie_app/features/home/domain/use_cases/save_movies_use_case.dart'
    as _i11;
import 'package:my_movie_app/features/home/presentation/bloc/home_bloc.dart'
    as _i15;

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  _i1.GetIt init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    final networkModule = _$NetworkModule();
    gh.lazySingleton<_i3.Dio>(() => networkModule.dio);
    gh.factory<_i4.DioHelper>(() => _i4.DioHelper());
    gh.lazySingleton<_i5.HomeLocalDataSource>(
        () => _i6.HomeLocalDataSourceImpl());
    gh.lazySingleton<_i7.HomeRemoteDataSource>(
        () => _i8.HomeRemoteDataSourceImpl(gh<_i3.Dio>()));
    gh.lazySingleton<_i9.HomeRepository>(() => _i10.HomeRepositoryImpl(
          gh<_i7.HomeRemoteDataSource>(),
          gh<_i5.HomeLocalDataSource>(),
        ));
    gh.factory<_i11.SaveMoviesUseCase>(
        () => _i11.SaveMoviesUseCase(gh<_i9.HomeRepository>()));
    gh.factory<_i12.FetchDetailPopularMovieUseCase>(
        () => _i12.FetchDetailPopularMovieUseCase(gh<_i9.HomeRepository>()));
    gh.factory<_i13.FetchPopularMoviesUseCase>(
        () => _i13.FetchPopularMoviesUseCase(gh<_i9.HomeRepository>()));
    gh.factory<_i14.GetMoviesUseCase>(
        () => _i14.GetMoviesUseCase(gh<_i9.HomeRepository>()));
    gh.factory<_i15.HomeBloc>(() => _i15.HomeBloc(
          gh<_i13.FetchPopularMoviesUseCase>(),
          gh<_i12.FetchDetailPopularMovieUseCase>(),
          gh<_i11.SaveMoviesUseCase>(),
          gh<_i14.GetMoviesUseCase>(),
        ));
    return this;
  }
}

class _$NetworkModule extends _i16.NetworkModule {}
