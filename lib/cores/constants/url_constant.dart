class URLConstant {
  URLConstant._();

  static const String baseURL = 'https://api.themoviedb.org/3/movie';
  static const String baseImageURL = 'https://image.tmdb.org/t/p/w500';

  /// Popular movies
  static const String popularMovies = '/popular';
}
