import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:my_movie_app/cores/helpers/dio_helper.dart';

@module
abstract class NetworkModule {
  @lazySingleton
  Dio get dio => DioHelper.dio!;
}
