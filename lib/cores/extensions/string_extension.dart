import 'package:my_movie_app/cores/constants/url_constant.dart';

extension StringX on String {
  String get imageURL {
    return '${URLConstant.baseImageURL}$this';
  }
}
