import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static const Color primaryColor = Color(0xFF0e1d6e);
  static const Color secondaryColor = Color(0xFF12993f);
}
